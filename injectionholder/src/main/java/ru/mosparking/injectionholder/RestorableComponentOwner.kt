package ru.mosparking.injectionholder


interface RestorableComponentOwner<SavedStateType, T> : ComponentOwner<T> {
    override fun provideComponent(): T = provideComponent(null)
    fun provideComponent(savedStateType: SavedStateType?): T
}