package ru.mosparking.injectionholder

internal inline fun <reified T> genericCastOrNull(any: Any): T? = any as? T