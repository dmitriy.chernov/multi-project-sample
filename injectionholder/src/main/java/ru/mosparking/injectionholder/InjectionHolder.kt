package ru.mosparking.injectionholder

import android.app.Application
import ru.mosparking.injectionholder.callback.ComponentCallback
import ru.mosparking.injectionholder.registry.LifecycleCallbackRegistryImpl
import ru.mosparking.injectionholder.registry.LifecycleCallbacksRegistry
import ru.mosparking.injectionholder.storage.ComponentsStore

class InjectionHolder private constructor(
    private val lifecycleCallbacksRegistry: LifecycleCallbacksRegistry
) {

    companion object {
        @JvmStatic
        val instance by lazy { InjectionHolder(LifecycleCallbackRegistryImpl()) }

        fun init(app: Application) {
            instance.lifecycleCallbacksRegistry.registerLifecycleCallbacks(
                app,
                instance.componentCallback
            )
        }
    }

    private val componentsStore by lazy { ComponentsStore() }
    private val componentCallback by lazy { ComponentCallback(componentsStore) }

    fun <T> getComponentOwnerLifeCycle(componentOwner: ComponentOwner<T>): ComponentOwnerLifecycle =
        componentCallback.getCustomOwnerLifecycle(componentOwner)

    fun <T> findComponent(componentClass: Class<T>): T? =
        componentsStore.findComponent(componentClass)

}