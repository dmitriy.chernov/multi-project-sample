package ru.mosparking.injectionholder.storage

internal class ComponentsStore {

    private val components = mutableMapOf<String, Any>()

    operator fun get(key: String): Any? = components[key]

    fun add(key: String, component: Any) {
        components[key] = component
    }

    fun remove(key: String) {
        components.remove(key)
    }

    fun isExist(key: String) = components.containsKey(key)

    @Suppress("UNCHECKED_CAST")
    fun <T> findComponent(searchedClass: Class<T>): T? =
        components.values.firstOrNull { it.isSameClass(searchedClass) } as? T

    private fun Any.isSameClass(otherClass: Class<*>): Boolean =
        otherClass.isAssignableFrom(javaClass) || javaClass.isAssignableFrom(otherClass)
}