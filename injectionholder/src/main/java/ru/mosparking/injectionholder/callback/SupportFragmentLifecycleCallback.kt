package ru.mosparking.injectionholder.callback

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import ru.mosparking.injectionholder.ComponentOwner

internal class SupportFragmentLifecycleCallback(
    private val componentCallback: ComponentCallback,
    private val fragmentStateStore: FragmentStateStore
) : FragmentManager.FragmentLifecycleCallbacks() {

    init {
        println("SupportFragmentLifecycleCallback init: $this")
    }

    override fun onFragmentPreAttached(fm: FragmentManager, f: Fragment, context: Context) {
        println("onFragmentPreAttached, fm: $fm, f: $f, context: $context")
        super.onFragmentPreAttached(fm, f, context)
    }

    override fun onFragmentAttached(fm: FragmentManager, f: Fragment, context: Context) {
        println("onFragmentAttached, fm: $fm, f: $f, context: $context")
        super.onFragmentAttached(fm, f, context)
    }

    override fun onFragmentPreCreated(
        fm: FragmentManager,
        f: Fragment,
        savedInstanceState: Bundle?
    ) {
        println("onFragmentPreCreated, fm: $fm, f: $f, savedInstanceState: $savedInstanceState")
        super.onFragmentPreCreated(fm, f, savedInstanceState)
        addInjectionIfNeed(f, savedInstanceState)
    }

    override fun onFragmentCreated(fm: FragmentManager, f: Fragment, savedInstanceState: Bundle?) {
        println("onFragmentCreated, fm: $fm, f: $f, savedInstanceState: $savedInstanceState")
        super.onFragmentCreated(fm, f, savedInstanceState)
    }

    override fun onFragmentViewCreated(
        fm: FragmentManager,
        f: Fragment,
        v: View,
        savedInstanceState: Bundle?
    ) {
        println("onFragmentViewCreated, fm: $fm, f: $f, v: $v, savedInstanceState: $savedInstanceState")
        super.onFragmentViewCreated(fm, f, v, savedInstanceState)
    }

    override fun onFragmentActivityCreated(
        fm: FragmentManager,
        f: Fragment,
        savedInstanceState: Bundle?
    ) {
        println("onFragmentActivityCreated, fm: $fm, f: $f, savedInstanceState: $savedInstanceState")
        super.onFragmentActivityCreated(fm, f, savedInstanceState)
    }

    override fun onFragmentStarted(fm: FragmentManager, f: Fragment) {
        println("onFragmentStarted, fm: $fm, f: $f")
        super.onFragmentStarted(fm, f)
        clearSaveState(f)
    }

    override fun onFragmentResumed(fm: FragmentManager, f: Fragment) {
        println("onFragmentResumed, fm: $fm, f: $f")
        super.onFragmentResumed(fm, f)
        clearSaveState(f)
    }

    override fun onFragmentPaused(fm: FragmentManager, f: Fragment) {
        println("onFragmentPaused, fm: $fm, f: $f")
        super.onFragmentPaused(fm, f)
    }


    override fun onFragmentStopped(fm: FragmentManager, f: Fragment) {
        println("onFragmentStopped, fm: $fm, f: $f")
        super.onFragmentStopped(fm, f)
    }

    override fun onFragmentSaveInstanceState(fm: FragmentManager, f: Fragment, outState: Bundle) {
        println("onFragmentSaveInstanceState, fm: $fm, f: $f, outState: $outState")
        super.onFragmentSaveInstanceState(fm, f, outState)
        saveFragmentState(f)
    }

    override fun onFragmentViewDestroyed(fm: FragmentManager, f: Fragment) {
        println("onFragmentViewDestroyed, fm: $fm, f: $f")
        super.onFragmentViewDestroyed(fm, f)
    }

    override fun onFragmentDestroyed(fm: FragmentManager, f: Fragment) {
        println("onFragmentDestroyed, fm: $fm, f: $f")
        super.onFragmentDestroyed(fm, f)
        removeInjectionIfNeed(f)
    }

    override fun onFragmentDetached(fm: FragmentManager, f: Fragment) {
        println("onFragmentDetached, fm: $fm, f: $f")
        super.onFragmentDetached(fm, f)
    }

    private fun addInjectionIfNeed(fragment: Fragment?, savedInstanceState: Bundle?) {
        println("addInjectionIfNeed, fragment: $fragment, savedInstanceState: $savedInstanceState")
        if (fragment is ComponentOwner<*>) {
            if (!isInSaveState(fragment)) {
                componentCallback.removeInjection(fragment)
            }
            componentCallback.addInjection(fragment, savedInstanceState ?: fragment.arguments)
        }
    }

    private fun removeInjectionIfNeed(fragment: Fragment?) {
        println("removeInjectionIfNeed, fragment: $fragment")
        if (fragment !is ComponentOwner<*>) {
            return
        }

        if (fragment.activity?.isFinishing == true) {
            if (!isInSaveState(fragment)) {
                clearInjection(fragment)
            }
            return
        }

        if (isInSaveState(fragment)) {
            return
        }

        var anyParentIsRemoving = false
        var parent = fragment.parentFragment
        while (!anyParentIsRemoving && parent != null) {
            anyParentIsRemoving = parent.isRemoving
            parent = parent.parentFragment
        }
        if (fragment.isRemoving || anyParentIsRemoving) {
            clearInjection(fragment)
        }
    }

    private fun clearInjection(fragment: Fragment) {
        println("clearInjection, fragment: $fragment")
        (fragment as? ComponentOwner<*>)?.apply { componentCallback.removeInjection(this) }
        clearSaveState(fragment)
    }

    private fun isInSaveState(fragment: Fragment): Boolean =
        fragmentStateStore.getSaveState(fragment)
            .also {
                println("isInSaveState, fragment: $fragment, it: $it")
            }

    private fun saveFragmentState(fragment: Fragment) =
        fragmentStateStore.setSaveState(fragment, true)
            .also {
                println("saveFragmentState, fragment: $fragment")
            }

    private fun clearSaveState(fragment: Fragment) =
        fragmentStateStore.setSaveState(fragment, false)
            .also {
                println("clearSaveState, fragment: $fragment")
            }
}