package ru.mosparking.injectionholder.callback

import android.app.Activity
import android.app.Application
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import ru.mosparking.injectionholder.ComponentOwner

internal class ActivityLifecycleCallback(
    private val componentCallback: ComponentCallback
) : Application.ActivityLifecycleCallbacks {

    private companion object {
        const val IS_FIRST_LAUNCH = "ru.mosparking.injectionholder.callback.IS_FIRST_LAUNCH"
    }

    private fun isFirstLaunch(outState: Bundle?): Boolean =
        outState?.getBoolean(IS_FIRST_LAUNCH, true) ?: true

    private fun setFirstLaunch(outState: Bundle?, isFirstLaunch: Boolean) {
        outState?.putBoolean(IS_FIRST_LAUNCH, isFirstLaunch)
    }

    private fun addInjectionIfNeed(activity: Activity, outState: Bundle?) {
        if (activity is ComponentOwner<*>) {
            if (isFirstLaunch(outState)) {
                componentCallback.removeInjection(activity)
            }
            componentCallback.addInjection(activity, outState)
        }

        (activity as? AppCompatActivity)
            ?.supportFragmentManager
            ?.registerFragmentLifecycleCallbacks(
                SupportFragmentLifecycleCallback(componentCallback, FragmentStateStore.instance),
                true
            )
    }

    private fun removeInjectionIfNeed(activity: Activity) {
        if (activity is ComponentOwner<*> && activity.isFinishing) {
            componentCallback.removeInjection(activity)
        }
    }

    override fun onActivityCreated(activity: Activity, outState: Bundle?) {
        println("onActivityCreated, activity: $activity, outState: $outState")
        addInjectionIfNeed(activity, outState)
    }

    override fun onActivityStarted(activity: Activity) {
        println("onActivityStarted, activity: $activity")
    }

    override fun onActivityResumed(activity: Activity) {
        println("onActivityResumed, activity: $activity")
    }

    override fun onActivityPaused(activity: Activity) {
        println("onActivityPaused, activity: $activity")
        removeInjectionIfNeed(activity)
    }

    override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle?) {
        println("onActivitySaveInstanceState, activity: $activity, outState: $outState")
        setFirstLaunch(outState, isFirstLaunch = false)
    }

    override fun onActivityStopped(activity: Activity) {
        println("onActivityStopped, activity: $activity")
    }

    override fun onActivityDestroyed(activity: Activity) {
        println("onActivityDestroyed, activity: $activity")
    }

}