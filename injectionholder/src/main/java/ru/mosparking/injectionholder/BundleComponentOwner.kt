package ru.mosparking.injectionholder

import android.os.Bundle

interface BundleComponentOwner<T> : RestorableComponentOwner<Bundle, T>