val url = "https://docs.fabit.ru/webapi/entry.cgi?api=SYNO.Chat.External&method=incoming&version=2&token=%22tm0lIPuPsW21AJIlkOVGfhLEvnAerYBxxif9OZFrEs79fvHjdJ1RmI3qT8OmXvgW%22"

sendMessageToChat(message = args.joinToString(separator = " "))

fun sendMessageToChat(message: String) {
    println("try to send message... text is '$message'")

    val command =
            "curl -X " +
                    "POST -H '" +
                    "Content-Type: application/json' " +
                    "-d " +
                    "'payload={\"text\": \"$message\"}' " +
                    "\"$url\""

    try {
        Runtime.getRuntime().exec(arrayOf("bash", "-c", command))
    } catch (e: Exception) {
        e.printStackTrace()
    }

}