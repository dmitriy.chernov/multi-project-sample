package com.github.chernovdmitriy.feature3_impl

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.github.chernovdmitriy.feature3_api.Feature3Object
import com.github.chernovdmitriy.feature3_impl.di.Feature3Component
import com.github.chernovdmitriy.feature3_impl.di.Feature3ComponentProvider
import kotlinx.android.synthetic.main.fmt_feature3.*
import ru.mosparking.injectionholder.BundleComponentOwner
import ru.mosparking.injectionholder.InjectionHolder
import javax.inject.Inject

class Feature3Fragment : Fragment(), BundleComponentOwner<Feature3Component> {

    companion object {
        @JvmStatic
        fun newInstance() = Feature3Fragment()

        @JvmStatic
        fun bundle() = Bundle()

        private const val SAVED_STATE_STRING_KEY = "saved_state_string_key"
    }

    @Inject
    lateinit var feature3Object: Feature3Object

    private val argumentSampleKey: String by lazy { getString(R.string.argument_sample_key) }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        inflater.inflate(R.layout.fmt_feature3, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        textView.text = "feature3Object: $feature3Object"
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString(
            argumentSampleKey,
            feature3Object.savedStateString + "(new saved string)"
        )
    }

    override fun inject(t: Feature3Component) = t.inject(this)

    override fun provideComponent(savedStateType: Bundle?): Feature3Component {
        return InjectionHolder.instance.findComponent(Feature3Component::class.java)
            ?: run {
                val savedStateString = savedStateType?.getString(argumentSampleKey)
                Feature3ComponentProvider.getInstance(savedStateString).feature3Component
            }
    }
}