package com.github.chernovdmitriy.feature3_impl.di

import javax.inject.Inject

class Feature3ComponentProvider private constructor() {

    @Inject
    lateinit var feature3Component: Feature3Component

    companion object {
        private val instance = Feature3ComponentProvider()

        var injectionFunction: (Feature3ComponentProvider.(savedState: String?) -> Unit)? = null

        fun getInstance(savedState: String?): Feature3ComponentProvider {
            injectionFunction?.invoke(instance, savedState)
            return instance
        }

    }

}