import java.io.File

val path: String = args[0]

parseHtmlResults(path)

fun parseHtmlResults(path: String) {
    val filePath = "$path/index.html"

    val html = File(filePath).readText()

    val divStartText = "<div class=\"percent\">"
    val divEndText = "%</div>"
    val startIndex = html.indexOf(string = divStartText, startIndex = 0, ignoreCase = true)
    val endIndex = html.indexOf(string = divEndText, startIndex = startIndex, ignoreCase = true)

    val percentText = html.substring(startIndex + divStartText.length, endIndex)

    val message = generateMessage(filePath, percentText)

    val scriptCommand = "kotlinc -script fastlane/sharemessage.kts $message"

    println("scriptCommand is $scriptCommand")
    Runtime.getRuntime().exec(scriptCommand)
}

fun generateMessage(filePath: String, percentText: String): String {

    percentText.replace("%", "").toIntOrNull()?.let { percentCount ->
        return when (percentCount) {
            100 -> "Все тесты завершены успешно ($filePath)"
            else -> "Только $percentCount％ тестов завершено успешно ($filePath)"
        }
    }
    return "Ошибка при чтении отчета тестов ($filePath)"
}