import java.io.BufferedReader

val remotePath = "remotes/origin"

automerge()

fun automerge() {
    executeCommands("git checkout -- .")
    val featureGitBranches = getFeatureBranches()
    println("feature git branches: ${featureGitBranches}")
    featureGitBranches.forEach { remoteFeatureBranch ->
        mergeWithDevelopAndPush(remoteFeatureBranch)
    }
}

fun getFeatureBranches(): Set<String> {
    val gitBranches = executeCommands("git branch -a")
    return gitBranches
            .asSequence()
            .filter { it.contains("$remotePath/feature") }
//            .filter { it.contains("27455") }
            .map { it.trim() }
            .toSet()
}

fun mergeWithDevelopAndPush(remotebranch: String) {
    val localBranch = remotebranch.replace("$remotePath/", "")
    switchToBranchAndPull(localBranch)
    val mergeWithDevResult = executeCommands("git merge $remotePath/develop")
    val haveConflict = mergeWithDevResult.contains("CONFLICT")
    if (haveConflict) {
        handleDevelopMergeFailure(localBranch)
    } else {
        handleDevelopMergeSuccess(localBranch)
    }
}

fun handleDevelopMergeSuccess(localBranch: String) {
//    val author = getAuthorOfLastCommit(localBranch)
    pushChangesAndDeleteBranch(localBranch)
//    shareMessage(message = "$author, в ветку $localBranch подлили dev, все огонь")
}

fun handleDevelopMergeFailure(localBranch: String) {
    val author = getAuthorOfLastCommit(localBranch)
    abortMergeAndDeleteBranch(localBranch)
    shareMessage(message = "$author, у тебя мерж конфликт в ветке $localBranch")
}

fun switchToBranchAndPull(localBranch: String): List<String> {
    return executeCommands(
            "git checkout -b $localBranch",
            "git checkout -$localBranch",
            "git pull origin $localBranch"
    )
}

fun pushChangesAndDeleteBranch(localBranch: String): List<String> {
    return executeCommands(
            "git push origin $localBranch",
            "git checkout -b develop",
            "git checkout develop",
            "git branch -d $localBranch"
    )
}

fun abortMergeAndDeleteBranch(localBranch: String): List<String> {
    return executeCommands(
            "git merge --abort",
            "git checkout -b develop",
            "git checkout develop",
            "git branch -d $localBranch"
    )
}

fun getAuthorOfLastCommit(localBranch: String): String {
    return getResultFromCommand("git log -1 --pretty=format:'%an'")
}

fun shareMessage(message: String): String {
    val command = "kotlinc -script fastlane/sharemessage.kts $message"
    println("try to share message by command: $command")
    return getResultFromCommand(command)
}

fun executeCommands(vararg commands: String): List<String> {
    val result = ArrayList<String>()
    for (command in commands) {
        result.addAll(getResultArrayFromCommand(command))
    }
    return result
}

fun getResultFromCommand(command: String): String {
    val process = Runtime.getRuntime().exec(arrayOf("bash", "-c", command))
    return process.inputStream.bufferedReader().use(BufferedReader::readText)
}

fun getResultArrayFromCommand(command: String): List<String> {
    val process = Runtime.getRuntime().exec(arrayOf("bash", "-c", command))
    return process.inputStream.bufferedReader().use(BufferedReader::readLines)
}
